import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UsersService } from "./users.service";
import { HttpModule } from '@angular/http';
import { AddFormComponent } from './users/add-form/add-form.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UpdateFormComponent } from './users/update-form/update-form.component';
import { UserComponent } from './users/user/user.component';
@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    ProductsComponent,
    NotFoundComponent,
    NavigationComponent,
    AddFormComponent,
    UpdateFormComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule.forRoot([
      {path: '', component: UsersComponent}, // דף ראשי
      {path: 'users', component: UsersComponent},
      {path: 'products', component: ProductsComponent},
      {path: 'user/:id', component: UserComponent},
      {path: 'update-form/:id', component: UpdateFormComponent},

      {path: '**', component: NotFoundComponent}//להשאיר בכל מקרה
    ])

  ],
 providers: [UsersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
