import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { UsersService } from "../../users.service";
import { FormGroup, FormControl, FormBuilder } from "@angular/forms";

  


@Component({
  selector: 'add-form',
  templateUrl: './add-form.component.html',
  styleUrls: ['./add-form.component.css']
})
export class AddFormComponent implements OnInit {
  @Output() addUser: EventEmitter < any > = new EventEmitter<any>();
  @Output() addUserPs:EventEmitter<any> = new EventEmitter<any>();
  service:UsersService;
  addform = new FormGroup({
      username:new FormControl(),
      phonenumber:new FormControl()
  });

   //שליחת העדכון דרך הסרוויס 
  //username;
  //email;
  sendData() {
    //השורה שולחת את התוכן לאופטימיסטיק שנמצא באב
    //this.username = this.addform.value.username;
    //this.email = this.addform.value.email;
    this.addUser.emit(this.addform.value.username); //עדכון לאב בהתאם לשדות שהגדרנו למעלה
    //this.addUser.emit(this.addform.value.email); //עדכון לאב בהתאם לשדות שהגדרנו למעלה 
    
    console.log(this.addform.value);
    this.service.postUsers(this.addform.value).subscribe(
      response => {
        console.log(response.json());
        this.addUserPs.emit();
      }
    );
  }
  constructor(service: UsersService, private formBuilder:FormBuilder) { 
    this.service = service;
  }



  ngOnInit() {
  }

}
