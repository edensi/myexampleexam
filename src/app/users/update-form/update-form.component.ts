import { Component, OnInit , Output , EventEmitter} from '@angular/core';
import { UsersService } from './../../users.service';
import {FormGroup , FormControl, FormBuilder} from '@angular/forms';
 import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {
  @Output() updateUser:EventEmitter<any> = new EventEmitter<any>(); 
  @Output() updateUserPs:EventEmitter<any> = new EventEmitter<any>();
  //Q4 fix
  username;
  phonenumber;

  service:UsersService;
  user;
  updateform = new FormGroup({
    username:new FormControl(),
    phonenumber:new FormControl()
  });

  //Q4 fix router
  constructor(private route: ActivatedRoute ,service: UsersService, private formBuilder: FormBuilder, private router: Router) {
    this.service = service;
  }

  sendData() {
    this.updateUser.emit(this.updateform.value.username);
    console.log(this.updateform.value);

    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      this.service.putUser(this.updateform.value, id).subscribe(
        response => {
          console.log(response.json());
          this.updateUserPs.emit();
          //Q4 fix
          this.router.navigate(['/']);
        }
      );
    })
  }
  
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user);
        //Q4 fix
        this.username = this.user.username
        this.phonenumber = this.user.phone  
      })
    })
  }
}
