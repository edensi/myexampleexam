import { Component, OnInit } from '@angular/core';
import { UsersService } from "../users.service";

@Component({
  selector: 'users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
users;
usersKeys;

 constructor(private service:UsersService) {
    service.getUsers().subscribe(
      response=>{
        this.users = response.json();
        this.usersKeys = Object.keys(this.users);
    });
  }

  //דרך אופטימית להוספת משתמש
  optimisticAdd(user){
    //console.log("addMessage work "+message); בדיקה
    var newKey = parseInt(this.usersKeys[this.usersKeys.length - 1],0) + 1;
    var newUserObject = {};
    newUserObject['username'] = user; //גוף ההודעה עצמה
    this.users[newKey] = newUserObject;
    this.usersKeys = Object.keys(this.users);
  }

  //דרך פסימית להוספת משתמש
  pessimisticAdd(){
    this.service.getUsers().subscribe(
      response=>{
        this.users = response.json();
        this.usersKeys = Object.keys(this.users);
    })
  }
  deleteUser(key){
    console.log.apply(key);
    let index = this.usersKeys.indexOf(key);
    this.usersKeys.splice(index,1); //אחד מבטא מחיקת רשומה אחת

    //delete from server
    this.service.deleteUser(key).subscribe(
      response=>console.log(response)
    );
  }



  ngOnInit() {
  }

}
