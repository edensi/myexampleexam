import {Injectable } from '@angular/core';
import{Http,Headers} from '@angular/http';
import{HttpParams} from '@angular/common/http';
@Injectable()
export class UsersService {
http:Http;


getUsers(){
 return this.http.get('http://localhost/angular/slim/users');
          }
    postUsers(data){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
    let params = new HttpParams().append('username',data.username).append('phonenumber',data.phonenumber);
    return this.http.post('http://localhost/angular/slim/users', params.toString(),options);
  }
  
  deleteUser(key){
  return this.http.delete('http://localhost/angular/slim/users/'+ key);
}

constructor(http:Http){
  this.http = http;
  }

 //Get one user
 getUser(id){
  return this.http.get('http://localhost/angular/slim/users/'+ id);
}
//Put
putUser(data,key){
 let options = {
   headers: new Headers({
     'content-type':'application/x-www-form-urlencoded'
   })
 }
 //בגרש נגדיר את השדות בהתאם לערכים שהגדרנו בשרת
 let params = new HttpParams().append('username',data.username).append('phonenumber',data.phonenumber);
 return this.http.put('http://localhost/angular/slim/users/'+ key,params.toString(), options);
}



}

  


